import sys
import sqlite3
from datetime import datetime

from db import *
import add_tutorial

if '--backup' in sys.argv or '-b' in sys.argv:
    try:
        # Get the old data
        old = open('database.db', 'rb').read()
        # Get a current timestamp
        timestamp = '.'.join(str(datetime.now()).split('.')[0].split())
        # Write the data to a backup
        open('backups/database.db.{}'.format(timestamp), 'wb').write(old)
    except OSError:
        print('[Warning] No database to backup.')

# Create the file, wiping all existing data
open('database.db', 'w').close()

# Get a connection to the database
conn = sqlite3.connect('database.db')
cur = conn.cursor()

# post_date is the join date, it just shortens the database api when it's named this
cur.execute('''
CREATE TABLE users (
    id INTEGER NOT NULL UNIQUE,
    username TEXT NOT NULL UNIQUE,
    password TEXT NOT NULL,
    post_date DATE NOT NULL,
    is_mod BOOLEAN,
    signature TEXT,
    PRIMARY KEY (id)
);
'''
)

cur.execute('''
CREATE TABLE comments (
    id INTEGER NOT NULL,
    userID INTEGER NOT NULL,
    tutorialID INTEGER NOT NULL,
    post_date DATE NOT NULL,
    content TEXT,
    PRIMARY KEY (id)
);
'''
)

cur.execute('''
CREATE TABLE thread_topics (
    id INTEGER NOT NULL,
    title TEXT NOT NULL UNIQUE,
    blurb TEXT NOT NULL,
    PRIMARY KEY (id)
);
'''
)

cur.execute('''
CREATE TABLE threads (
    id INTEGER NOT NULL,
    userID INTEGER NOT NULL,
    post_date DATE NOT NULL,
    topicID INTEGER NOT NULL,
    title TEXT NOT NULL,
    PRIMARY KEY (id)
);
'''
)

cur.execute('''
CREATE TABLE messages (
    id INTEGER NOT NULL,
    userID INTEGER NOT NULL,
    post_date DATE NOT NULL,
    content TEXT NOT NULL,
    threadID INTEGER NOT NULL,
    PRIMARY KEY (id)
);
'''
)

cur.execute('''
CREATE TABLE tutorials (
    id INTEGER NOT NULL,
    title TEXT NOT NULL,
    post_date DATE NOT NULL,
    content TEXT NOT NULL,
    topicID INTEGER NOT NULL,
    PRIMARY KEY (id)
);
'''
)

if '--init-db' in sys.argv:
    # Create the admin account
    admin = User("root", "god is good all the time and all the time god is good", "\"What I cannot create, I do not understand.\" - Richard Feynmann", is_mod=True)
    admin.save()

    print('Adding Thread topics...')
    topic1 = ThreadTopic("General Discussion", "This forum is for all miscellaneous discussion, related to use of the A-Pi, or not.")
    topic2 = ThreadTopic("Device Discussion", "This forum is here for discussion related to use of the A-Pi device, or additional hardware modules. Users wishing to make their own tutorials relating to hardware should post them here.")
    topic3 = ThreadTopic("Programming and Development", "A forum for all things programming and software design, related to the A-Pi or not. Community made extension modules should post updates here, and user made tutorials relating to code development should be placed here.")
    topic1.save()
    topic2.save()
    topic3.save()

    print('Adding tutorials...')
    add_tutorial.create_tutorial('../Stage 2/tutorial_file_template.txt')
    print('Initial database populated. Database is ready for use.')


if '--test' in sys.argv:
    if not '-y' in sys.argv and input('Begin tests [Y/N]? ') == 'N':
        sys.exit()

    print('='*80)
    print('Beginning database tests...')

    print('Generating new User in the database...')
    print('User values should be:', {"password" : "toor", "username" : "root", "signature" : "\"What I cannot create, I do not understand.\" - Richard Feynmann"})
    admin = User("root", "toor", "\"What I cannot create, I do not understand.\" - Richard Feynmann", is_mod=True)
    print('Saving a new user...')
    admin.save()
    print(admin)
    print('User created and saved successfully.')

    print('Fetching User from the database...')
    user = User.get(admin.values['id'])
    print('Displaying the fetched values from the fetched user...')
    print(user)
    if not '-y' in sys.argv and input('Please confirm values are correct[Y/N]: ') == 'N':
        raise ValueError("Saved values for User object are incorrect.")

    print('Attempting to change User password...')
    user.set_password('meh')
    user.save()
    print('User object updated and saved correctly.')

    print('='*80)

    print('Adding Thread topics...')
    topic1 = ThreadTopic("Device Discussion", "This forum is here for discussion related to use of the A-Pi, additional modifications or user made tutorials.")
    topic2 = ThreadTopic("Device Discussion2", "This forum is here for discussion related to use of the A-Pi, additional modifications or user made tutorials.")
    print(topic1, '\n', topic2)
    print('Saving topics...')
    topic1.save()
    topic2.save()
    print('Topics saved.')
    print('Fetching second topic...')
    topic3 = ThreadTopic.get(topic2.values['id'])
    print('Displaying fetched values to the user...')
    print(topic2)
    if not '-y' in sys.argv and input('Please confirm values are correct[Y/N]: ') == 'N':
        raise ValueError('Original and saved values do not match.')
    print('Editing fetched topic...')
    topic3.values['title'] = 'Programming and Development'
    topic3.values['blurb'] = "A forum for all things programming and hardware design, related to the A-Pi or not. Community made extensions should post updates here, and user made tutorials relating to code development should be placed here."
    print('Updating topic values in the database...')
    topic3.save()
    print('Topic object updated and saved correctly.')

    print('='*80)

    print('Creating a new thread in the topic...')
    thread1 = Thread(1, 1, 'Thread 1')
    message1 = Message(1, '''This is the content of the first post in this thread, and it will generally be quite extensive.
    However, this box will cut it off after a certain point if the post is too long.
    This box will even put a little gradient fade at the bottom to make it look nice as the text fades into oblivion.''', 1)
    print('Saving thread...')
    thread1.save()
    message1.save()
    print('Thread created.')

    print('='*80)

    print('Adding tutorial test...')
    tut1 = Tutorial("title", "content. More substantial content. I hope this works. Why wont you work? Grr..", 1)
    tut3 = Tutorial("title3", "content. More substantial content. I hope this works. Why wont you work? Grr..", 1)
    tut2 = Tutorial("title2", "content.\nMore substantial content.\nI hope this works. Why wont you work? Grr..", 2)
    t = Tutorial('Other Tutorial Title', 'This is perfectly original content that you will not have seen in any of the other storyboard frames thus far.\nI hope you thoroughly enjoy this amazing random text that serves no purpose to the site overall, and is only here so that I can fill out this box in this page of the website. In closing, I hope you have a nice day.', 2)
    print('Tutorial created. Testing saving...')
    t.save()
    tut1.save()
    tut2.save()
    tut3.save()
    print('All tutorials created and saved.')
    print('Testing fetch and deletion...')
    t = Tutorial.get(tut3.values['id'])
    t.delete()
    print('Fetch and delete completed successfully.')
    print('All tests completed. Database is ready for use.')
