import sqlite3

conn = sqlite3.connect('database.db')
cur = conn.cursor()

print('''SQL Shell v1.0
All commands entered will be run directly on the server's database file.
If you don't know what you are doing, you should leave right now.
The shell will exit when a blank line is entered, so simply press the Enter key to quit.''')
command = input('SQL >>> ')

while command:
    try:
        cur.execute(command)
        print(command)
        if command.lower().startswith('select'):
            for row in cur.fetchall():
                print(row)
    except Exception as e:
        print('Careful, an error occured:')
        print(e)
    command = input('SQL >>> ')
