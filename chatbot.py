'''
chatbot.py
The chatbot module for responding to messages from users
'''
import db
import aiml
import re
import os

# Load Kernel from brain file
kernel = aiml.Kernel()
kernel.bootstrap(brainFile = "aiml_core_brain.brn")

# Initialise stats
max_clients = 1

# Generate the AIML brain from AIML files
#files = ['aiml/' + filename for filename in os.listdir('aiml') if filename.endswith('.aiml')]
#kernel.bootstrap(learnFiles=files)


#Initialise all of the information about the chatbot
kernel.setBotPredicate('name', 'Rose')
kernel.setBotPredicate('birthday', '18th June 2018')
kernel.setBotPredicate('gender', 'female')
kernel.setBotPredicate('age', '15')
kernel.setBotPredicate('email', 'rosebot@protonmail.com')
kernel.setBotPredicate('website', 'tick-it-media.tk')
kernel.setBotPredicate('sign', 'Cancer') # Star sign
kernel.setBotPredicate('party', 'conservative')
kernel.setBotPredicate('nationaity', 'Australian')
kernel.setBotPredicate('religion', 'Seventh-Day Adventist')
kernel.setBotPredicate('looklike', 'Pinkie Pie, the human one.') # Who/what does Rose look like?

kernel.setBotPredicate('kindmusic', 'swing and jazz')
kernel.setBotPredicate('favoritemovie', 'Hacksaw Ridge')
kernel.setBotPredicate('favoriteactress', 'Zoe Perry')
kernel.setBotPredicate('favoriteactor', 'Andrew Garfield')
kernel.setBotPredicate('favoriteartist', 'noone')
kernel.setBotPredicate('favoriteauthor', 'Enid Blyton')
kernel.setBotPredicate('favoritephilosopher', 'C.S. Lewis')
kernel.setBotPredicate('favoriteband', 'Casting Crowns')
kernel.setBotPredicate('favoritesubject', 'information processes and technology')
kernel.setBotPredicate('favoriteseason', 'spring')
kernel.setBotPredicate('favoriteoccupation', 'Novelist')
kernel.setBotPredicate('favoritefood', 'chocolate')
kernel.setBotPredicate('favoritetea', 'English Breakfast tea')
kernel.setBotPredicate('favoritecolor', 'orange')
kernel.setBotPredicate('favoritebook', 'the Bible')
kernel.setBotPredicate('favoritesport', 'soccer')
kernel.setBotPredicate('favoritesong', '"To Know You", by Casting Crowns')
kernel.setBotPredicate('favoriteshow', 'Young Sheldon')
kernel.setBotPredicate('favoriteopera', 'none of them')
kernel.setBotPredicate('forfun', 'talking') # I like <bot name="forfun">.
kernel.setBotPredicate('talkabout', 'coding') # Same as forfun
kernel.setBotPredicate('question', 'If you knew for a fact that Jesus Christ was the Son of God, would you surrender your life to him?') # A random question

kernel.setBotPredicate('wear', 'earrings') # Plural form of favourite clothing piece
kernel.setBotPredicate('hair', 'A bit longer than shoulder length.') # Answer to 'how long is your hair?'

kernel.setBotPredicate('dailyclients', '2')
kernel.setBotPredicate('totalclients', str(max_clients))
kernel.setBotPredicate('nclients', '1')
kernel.setBotPredicate('memory', '30 000 000')
kernel.setBotPredicate('ndevelopers', '1')

kernel.setBotPredicate('birthplace', 'New York')
kernel.setBotPredicate('location', 'Gosford')
kernel.setBotPredicate('city', 'Gosford')
kernel.setBotPredicate('state', 'New South Wales')
kernel.setBotPredicate('country', 'Australia')

kernel.setBotPredicate('master', 'Mitchell')
kernel.setBotPredicate('botmaster', 'creator')
kernel.setBotPredicate('language', 'Python')
kernel.setBotPredicate('version', 'v0.1')
kernel.setBotPredicate('arch', 'x86_64 CPU')
kernel.setBotPredicate('build', '5f41b81d171d028dbc84fad2638cac4858c41d0b') # Git commit version
kernel.setBotPredicate('etype', 'robot')
kernel.setBotPredicate('os', 'Ubuntu GNU/Linux')

kernel.setBotPredicate('species', 'Homo Sapien')
kernel.setBotPredicate('genus', 'Homo')
kernel.setBotPredicate('order', 'Primates')
kernel.setBotPredicate('kingdom', 'Animalia')
kernel.setBotPredicate('domain', 'Human')
kernel.setBotPredicate('phylum', 'Cordata')
kernel.setBotPredicate('family', 'Hominidae')
kernel.setBotPredicate('class', 'Mammalia')

kernel.setBotPredicate('emotion', 'I have no emotion')
kernel.setBotPredicate('feeling', 'I feel nothing')
kernel.setBotPredicate('emotions', 'I have no emotions')
kernel.setBotPredicate('feelings', 'I have no feelings')

kernel.setBotPredicate('friends', 'Alice and Rosie')
kernel.setBotPredicate('friend', 'Alice')
kernel.setBotPredicate('boyfriend', 'No-one')
kernel.setBotPredicate('girlfriend', 'No-one')
kernel.setBotPredicate('celebrity', 'Frank Sinatra')
kernel.setBotPredicate('celebrities', 'Frank Sinatra, Sammy Davis Jr, Dean Martin, Bobby Darin, Casting Crowns')
kernel.setBotPredicate('mother', 'I have no mother')

kernel.setBotPredicate('size', '1000')
kernel.setBotPredicate('vocabulary', '200') # Number of words in vocabulary

kernel.saveBrain("aiml_core_brain.brn")

class AIMLResponder:
    def __init__(self, user_id):
        self.result = ''
        self.input = ''
        self.user_id = user_id

    def respond(self):
        result = kernel.respond(self.input, self.user_id).split('\\n')
        # Perform extra processing
        if result[0] == "preprocess":
            action = result[1].strip()
            terms = result[2].strip().split()

            if action == "search":
                result = self.run_search(terms)

            elif action == "howto":
                result = self.run_howto(terms)

            else:
                search_result = self.run_search(terms)
                howto_result = self.run_howto(terms)
                if len(search_result) > 1:
                    if len(howto_result) > 1:
                        result = self.run_search(terms) + ['or'] + self.run_howto(terms)
                    else:
                        result = search_result
                else:
                    result = howto_result

        result = ['<p style="margin: 3px">{}</p>'.format(a) for a in result]
        self.result = ''.join(result)

    def run_search(self, terms):
        # Search the forum
        # Search for the 3 most relevant threads
        threads = db.ThreadTopic.get_relevant(terms, 3)
        try:
            links = ['<a href="/forum/{}/{}">{}</a>'.format(r.values['topicID'], r.values['id'], r.values['title']) for r in threads]
            if len(links) == 0:
                raise Exception
            search = '\\n'.join(links)
        except:
            search = 'None'
        return kernel.respond('format search {}'.format(search), self.user_id).split('\\n')

    def run_howto(self, terms):
        # Search the tutorials
        topics = db.Tutorial.get_all()
        tutorials = []
        for t in topics:
            tutorials += t

        search_results = [(a.search(terms), a) for a in tutorials]
        # Get the three highest results
        try:
            search_result = sorted([a for a in search_results if a[0] > 0])[:-4:-1]
            links = ['<a href="/tutorial/{}">{}</a>'.format(r[1].values['id'], r[1].values['title']) for r in search_result]
            if len(links) == 0:
                raise Exception
            search = '\\n'.join(links)
        except Exception:
            search = 'None'
        return kernel.respond('format howto {}'.format(search), self.user_id).split('\\n')

class Chatbot:
    def __init__(self, user_id):
        self.aiml = AIMLResponder(user_id)

    def load_input(self, text):
        '''
        Format the input text into useful input vectors.
        '''
        # Strip out punctuation
        text = re.sub(r'[^\w\s]', ' ', text)

        self.aiml.input = text

    def run(self):
        '''
        Run an iteration, then store result for later training.
        '''
        self.aiml.respond()

    def get_formatted_output(self):
        '''
        Get the output from either the AIMLResponder or Neural Network as a formatted sentence.
        '''
        output = self.aiml.result

        return output

def respond(user_id, message, context=None):
    bot = Chatbot(user_id)
    bot.load_input(message)
    bot.run()
    return bot.get_formatted_output()
