'''
db.py
The database module for handling interfacing with the database file
'''
import sqlite3
import hashlib
import datetime
import html

conn = sqlite3.connect('database.db')
conn.execute('PRAGMA foreign_keys = ON')
conn.row_factory = sqlite3.Row
cur = conn.cursor()

# user
# thread topic
# thread
# message in thread
# tutorial
# comment on tutorial
# neural net

# Each needs a get (staticmethod), save and delete method

def format_date(datetime):
    if datetime is None:
        return None

    date, time = datetime.split()
    year, month, day = date.split('-')
    months = [
              'January', 'February', 'March', 'April',
              'May', 'June', 'July', 'August',
              'September', 'October', 'November', 'December'
             ]
    date = months[int(month) - 1] + ' ' + day + ' ' + year

    # Split the time into hours, minutes and seconds
    h, m, s = time.split(':')

    # Determine if the hour is AM or PM
    am_or_pm = 'AM' if int(h)%12 == int(h) else 'PM'
    # Pad the minutes
    m = '0' * (2 - len(m)) + m

    # Format the whole string together
    h = int(h) % 12 if h != '12' else 12
    return date + ', {}:{} {}'.format(h, m, am_or_pm)

def get_searches(term_list):
    comparison_terms = []
    for a in range(len(term_list)):
        for b in range(1, len(term_list) + 1 - a):
            comparison_terms.append(' '.join(term_list[a:a+b]))
    return comparison_terms

class DBObject:
    def __init__(self, tablename, **kwargs):
        if not tablename:
            raise ValueError('A table must be used for a database object.')
        self.tablename = tablename
        if not kwargs:
            print('Warning: no values in database object {}'.format(self))
        self.values = kwargs

        # If the post_date field is in the arguments, add a formatted version of the field as well.
        if 'post_date' in kwargs:
            self.values['format_date'] = format_date(kwargs['post_date'])
        # If an id has not been set, pop it if possible
        if self.values.get('id') is None and 'id' in self.values:
            self.values.pop('id')

    def __str__(self):
        return "{}({})".format(self.__class__.__name__, ', '.join(['{}={}'.format(a, self.values[a]) for a in self.values]))

    def save(self):
        '''
        Save this DBObject into its respective table in the database
        '''
        # Search the database for existing objects with the same id as this value
        query = 'SELECT * FROM {} WHERE id = ?;'.format(self.tablename)
        print(query)
        cur.execute(query, (self.values.get('id', -1),))
        row = cur.fetchone()
        if row:
            # Update the existing object
            # Collect and concatenate the keys and values for the query
            set_values = ', '.join(['{} = {}'.format(a[0], repr(a[1])) for a in self.values.items() if a[0] not in ['id', 'format_date']])
            # Execute the query on the database
            set_values = ', '.join(['{} = ?'.format(a) for a in self.values.keys() if a not in ['id', 'format_date']])
            query = 'UPDATE {} SET {} WHERE id = ?'.format(self.tablename, set_values)
            print(query)
            cur.execute(query, tuple([a[1] for a in self.values.items() if a[0] not in ['id', 'format_date']])+(self.values['id'], ))
            conn.commit()

        else:
            # Save the new object
            # Get the post date
            if 'post_date' in self.values and self.values['post_date'] is None:
                self.values['post_date'] = str(datetime.datetime.now())
            # Collect and concat the keys and values for the SQL command
            keys_to_use = [key for key in self.values.keys() if self.values[key] is not None and key not in ['id', 'format_date']]
            values_to_insert = [self.values[a] for a in keys_to_use]
            key_string = ', '.join(keys_to_use)
            # Execute the query on the database
            query = 'INSERT INTO {} ({}) VALUES ({})'.format(self.tablename, key_string, ', '.join(['?']*len(keys_to_use)))#, values_to_insert)
            print(query)
            cur.execute(query, values_to_insert)
            conn.commit()
            # Set the id based on the autocreated value from the database
            self.values['id'] = cur.lastrowid

    def delete(self):
        if self.values.get('id'):
            query = 'DELETE FROM {} WHERE id = ?'.format(self.tablename)
            cur.execute(query, (self.values['id'],))
            conn.commit()
        else:
            print('id is None. Failed to delete')

    @staticmethod
    def get(id, tablename, _class=None, **kwargs):
        if _class is None:
            _class = DBObject
        cur.execute('SELECT * FROM {} WHERE id = ?'.format(tablename), (id,))
        row = cur.fetchone()
        if row is not None:
            values = list(row)
            values = {row.keys()[a] : values[a] for a in range(len(values))}

            # Add any extra arguments from the function parameters
            values.update(kwargs)
            return _class(**values)
        return None

class User(DBObject):
    def __init__(self, username, password, signature, id=None, shouldHash=True, is_mod=False, post_date=None):
        # Hash the password
        if shouldHash:
            password = hashlib.sha256(password.encode()).hexdigest()
        super().__init__('users', username=username, password=password, id=id, signature=signature, is_mod=is_mod, post_date=post_date)

    def check_password(self, password):
        return hashlib.sha256(password.encode()).hexdigest() == self.values['password']

    def set_password(self, password):
        self.values['password'] = hashlib.sha256(password.encode()).hexdigest()

    def set_to_moderator(self):
        self.values['is_mod'] = True

    def is_moderator(self):
        return self.values.get('is_mod', False)

    @staticmethod
    def get(id):
        return DBObject.get(id, 'users', User, shouldHash=False)

    @staticmethod
    def get_by_username(username):
        cur.execute('SELECT * FROM users WHERE username = ?;', (username,))
        row = cur.fetchone()
        if row is not None:
            values = list(row)
            values = {row.keys()[a] : values[a] for a in range(len(values))}
            return User(shouldHash=False, **values)
        return None

class Comment(DBObject):
    def __init__(self, userID, content, tutorialID, post_date=None, id=None):
        super().__init__('comments', userID=userID, content=content, tutorialID=tutorialID, post_date=post_date, id=id)

        self.decode_content()

    def decode_content(self):
        string = self.values['content']
        decoded_string = ''
        while string:
            if string[:2] == '\\U':
                try:
                    decoded_string += string[:10].encode('unicode-escape').replace(b'\\\\', b'\\').decode('unicode-escape')
                except:
                    print(string)
                string = string[10:]
            else:
                decoded_string += string[0]
                string = string[1:]

        self.values['content'] = decoded_string

    def get_content(self):
        new_content = []
        for line in self.values['content'].split('\n'):
            new_content.append('<p class="text_sized">'+html.escape(line)+'</p>')
        return '\n'.join(new_content)

    @staticmethod
    def get(id):
        return DBObject.get(id, 'comments', Comment)

    @staticmethod
    def get_all(tutorial_id):
        cur.execute('SELECT id FROM comments WHERE tutorialID = {} ORDER BY post_date DESC'.format(tutorial_id))
        # Turn all of the comment ids into comment objects
        rows = cur.fetchall()
        # Fetch the comments
        comments = [Comment.get(a['id']) for a in rows]
        # Generate the tutorial, user tuple list
        comments = [(a, User.get(a.values['userID'])) for a in comments]

        return comments

class ThreadTopic(DBObject):
    def __init__(self, title, blurb, id=None):
        super().__init__('thread_topics', title=title, blurb=blurb, id=id)

    @staticmethod
    def get_relevant(terms, number):
        topics = ThreadTopic.get_all()
        threads = []
        for topic in topics:
            threads += Thread.get_all(topic.values['id'])

        # Sort all of the threads by relevance
        threads = sorted([(a.search(terms), a) for a in threads])
        # Cull the completely irrelevant ones, and then return the few most relevant
        return [a[1] for a in threads if a[0]][:-number-1:-1]

    @staticmethod
    def get(id):
        return DBObject.get(id, 'thread_topics', ThreadTopic)

    @staticmethod
    def get_all():
        # Select every topic
        cur.execute('SELECT id FROM thread_topics ORDER BY id ASC')
        # Turn all of the topic ids into topic objects
        rows = cur.fetchall()
        topics = [ThreadTopic.get(a['id']) for a in rows]

        return topics

class Thread(DBObject):
    def __init__(self, userID, topicID, title, post_date=None, id=None):
        super().__init__('threads', userID=userID, topicID=topicID, title=title, post_date=post_date, id=id)

        self.latest_post = Message.get_newest(id or -1)

    def search(self, terms):
        messages = Message.get_all(self.values['id'])
        return sum([m.search(terms) for m in messages])

    @staticmethod
    def get(id):
        return DBObject.get(id, 'threads', Thread)

    @staticmethod
    def get_all(topic_id):
        # Select every topic
        cur.execute('SELECT id FROM threads WHERE topicID = ? ORDER BY id ASC', (topic_id,))
        # Turn all of the topic ids into topic objects
        rows = cur.fetchall()
        threads = [Thread.get(a['id']) for a in rows]

        # Organise the data so that it can be sorted by date
        for i, t in enumerate(threads):
            post_date = Message.get_newest(t.values['id'])
            if post_date:
                post_date = post_date.values['post_date']
            threads[i] = (post_date, t)

        # Sort the threads by newest to oldest
        threads.sort(reverse=True)

        # Return just the thread objects
        return [a[1] for a in threads]

class Message(DBObject):
    def __init__(self, userID, content, threadID, post_date=None, id=None):
        super().__init__('messages', userID=userID, content=content, threadID=threadID, post_date=post_date, id=id)

        self.decode_content()

    def decode_content(self):
        string = self.values['content']
        decoded_string = ''
        while string:
            if string[:2] == '\\U':
                try:
                    decoded_string += string[:10].encode('unicode-escape').replace(b'\\\\', b'\\').decode('unicode-escape')
                except:
                    print(string)
                string = string[10:]
            else:
                decoded_string += string[0]
                string = string[1:]

        self.values['content'] = decoded_string

    def search(self, terms):
        search_score = 0
        to_search = get_searches(terms)
        for a in to_search:
            if a in self.values['content'].lower():
                search_score += len(a.split())**2
        return search_score

    def get_user(self):
        return User.get(int(self.values['userID']))

    def get_content(self):
        self.values['content'] = self.values['content'].replace('\r\n', '\n')
        new_content = []
        for line in self.values['content'].split('\n'):
            new_content.append('<p class="text_sized">'+line+'</p>')
        return '\n'.join(new_content)

    def get_preview(self):
        self.values['content'] = self.values['content'].replace('\r\n', '\n')
        new_content = []
        for line in self.values['content'].split('\n')[:4]:
            new_content.append('<p class="text_sized" style="margin: 5px 0;">'+line+'</p>')
        return '\n'.join(new_content)

    @staticmethod
    def get(id):
        return DBObject.get(id, 'messages', Message)

    @staticmethod
    def get_newest(thread_id):
        # Select every topic
        cur.execute('SELECT id FROM messages WHERE threadID = ? ORDER BY post_date DESC', (thread_id,))

        row = cur.fetchone()
        if row:
            return Message.get(row['id'])

    @staticmethod
    def get_all(thread_id):
        # Select every topic
        cur.execute('SELECT id FROM messages WHERE threadID = ? ORDER BY post_date ASC', (thread_id,))
        # Turn all of the message ids into message objects
        rows = cur.fetchall()
        messages = [Message.get(a['id']) for a in rows]

        return messages

class Tutorial(DBObject):
    def __init__(self, title, content, topicID, post_date=None, id=None):
        super().__init__('tutorials', title=title, content=content, topicID=topicID, post_date=post_date, id=id)

        self.decode_content()

    def __lt__(self, other):
        return isinstance(other, Tutorial) and self.values['post_date'] < other.values['post_date']

    def decode_content(self):
        string = self.values['content']
        decoded_string = ''
        while string:
            if string[:2] == '\\U':
                try:
                    decoded_string += string[:10].encode('unicode-escape').replace(b'\\\\', b'\\').decode('unicode-escape')
                except:
                    print(string)
                string = string[10:]
            else:
                decoded_string += string[0]
                string = string[1:]

        self.values['content'] = decoded_string

    def get_content(self):
        new_content = []
        for line in self.values['content'].split('\n'):
            new_content.append('<p class="text_sized">'+line+'</p>')
        return '\n'.join(new_content)

    def get_preview(self):
        new_content = []
        for line in self.values['content'].split('\n')[:2]:
            new_content.append('<p class="text_sized" style="margin: 5px 0;">'+line+'</p>')
        return '\n'.join(new_content)

    def search(self, terms):
        search_score = 0
        to_search = get_searches(terms)
        for a in to_search:
            if a in self.values['content'].lower():
                search_score += len(a.split())**2
        return search_score

    @staticmethod
    def get(id):
        return DBObject.get(id, 'tutorials', Tutorial)

    @staticmethod
    def get_all():
        # Select every tutorial
        cur.execute('SELECT id FROM tutorials ORDER BY topicID ASC')
        # Turn all of the tutorial ids into tutorial objects
        rows = cur.fetchall()
        tutorials = [Tutorial.get(a['id']) for a in rows]
        # Initialise an empty 2D array
        if len(tutorials) == 0:
            return []

        # Create a new tutorial array for each tutorial topic
        topics = [[] for a in range(tutorials[-1].values['topicID'])]
        for t in tutorials:
            # Populate the 2D array with the tutorial objects
            topics[t.values['topicID']-1].append(t)

        return topics
