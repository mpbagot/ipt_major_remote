import db
import sys

def create_tutorial(filename):
    try:
        f = open(filename)
    except FileNotFoundError:
        print('File:', '"' + filename + '"', 'couldn\'t be found.')
        return
    data = f.read().strip()
    parse_file(data)

def parse_file(data):
    lines = data.split('\n')
    tutorial_data = {}
    # Parse the file content
    for line in lines:
        if '|' in line[:15]:
            key, value = line.split('|')
            tutorial_data[key] = value.strip()
        else:
            tutorial_data['content'] += '\n' + line

    # Check that all required arguments are included
    required_args = ['content', 'title', 'topicID']
    failed = []
    for a in required_args:
        if tutorial_data.get(a) == None:
            failed += [a]

    if failed:
        print('Tutorial syntax is incorrect, missing value(s) "{}"'.format(failed))
        return

    if not tutorial_data['topicID'].isnumeric() or int(tutorial_data['topicID']) < 1:
        print('Topic ID is not a whole number and greater than 0.')
        return

    tutorial = db.Tutorial(
                            title=tutorial_data['title'],
                            content=tutorial_data['content'].strip(),
                            topicID=tutorial_data['topicID'],
                            post_date=tutorial_data.get('post_date')
                            )
    # print(tutorial_data)
    tutorial.save()

if __name__ == "__main__":
    filenames = sys.argv[1:]
    for filename in filenames:
        create_tutorial(filename)
