function postForumMessage() {
  form = document.getElementsByTagName('form')[0];

  if (form == undefined) {
    return;
  }

  // dateString comes from server
  // postContent comes from server
  // userID and username come from server
  xhr = new XMLHttpRequest();
  splitURL = document.URL.split('/');
  threadID = splitURL[splitURL.length-2];
  xhr.open('POST', '/send_message/'+threadID, true);
  xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");

  urlParams = 'reply_content='+encodeURI(document.getElementById('reply_content').value);

  xhr.onload = function() {
    fields = JSON.parse(this.responseText);
    dateString = fields.dateString;
    postContent = fields.postContent;
    userID = fields.userID;
    username = fields.username;

    // Generate the top level data
    messageBox = document.createElement('DIV');
    messageBox.classList.add('tutorial_box', 'tutorial_box_'+currentState);

    title = document.createElement('H3');
    title.classList.add('tutorial_title', 'tutorial_title_'+currentState, 'sidebar_button_'+currentState, 'text_'+currentState);
    title.innerText = 'Published: '+dateString;
    messageBox.appendChild(title);

    // Generate the table
    table = document.createElement('TABLE');
    tr = document.createElement('TR');

    // Create the user info cell
    cell1 = document.createElement('TD');
    cell1.classList.add('user_detail', 'header_'+currentState);
    nameText = document.createElement('P');
    nameText.innerText = username;
    nameText.classList.add('text_'+currentState)
    nameText.style.cssText = 'overflow-wrap: break-word';
    userImg = document.createElement('DIV');
    userImg.style.background = 'url(\'/static/img/user/' + userID + '/icon.jpg\') 50% 50% no-repeat';
    userImg.style.backgroundSize = 'cover';
    userImg.style.paddingBottom = '100%';
    userImg.style.borderRadius = '50%';

    cell1.appendChild(nameText);
    cell1.appendChild(userImg);

    // Create the message content cell
    cell2 = document.createElement('TD');
    cell2.classList.add('post_info', 'text_'+currentState);
    cell2.innerHTML = postContent;

    // Construct the message box
    tr.appendChild(cell1);
    tr.appendChild(cell2);
    table.appendChild(tr);
    messageBox.appendChild(table);


    // Place the new message in the right spot
    form.parentNode.insertBefore(messageBox, form);
    height = messageBox.offsetHeight;
    messageBox.style.maxHeight = '0';
    cell2.style.opacity = '0';
    console.log(height);
    setTimeout(function() {
      messageBox.classList.add('transition');
      cell2.classList.add('transition');
      messageBox.style.maxHeight = height.toString();
      cell2.style.opacity = '1';
    }, 200);

    form.reset()
  }

  xhr.send(urlParams);
}

function deleteMessage(event) {
  button = event.target;
  if (button.id == 'delete_check') {
    // Bring up the confirmation
    commentBox = button.parentNode.parentNode.parentNode.parentNode.parentNode;
    commentBox.style.transition = 'all 0.4s ease';
    commentBox.style.maxHeight = (commentBox.offsetHeight+50).toString();

    hider = button.nextElementSibling;
    hider.style.opacity = '1';
    hider.style.visibility = 'visible';

  } else {
    // Hide the confirmation
    hider = button.parentNode;
    hider.style.opacity = '0';
    setTimeout(function() {hider.style.visibility = 'hidden';}, 150);
  }

  if (button.id == 'confirm') {
    // Send the delete request, then delete the comment
    comment = button.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode;
    comment.style.maxHeight = '0';
    comment.style.opacity = '0';

    setTimeout(function() {comment.parentNode.removeChild(comment)}, 400);

    m = button.parentNode.id.split('_');
    m = m[m.length - 1];
    xhr = new XMLHttpRequest();
    xhr.open('POST', '/edit_message/' + m + '/delete', true);
    xhr.onload = function() {
      if (this.responseText != 'success') {
        console.log(this.responseText);
      }
    }
    xhr.send();
  }
}

function saveMessage(event) {
  button = event.target;
  button.classList.remove('sidebar_button_' + currentState);
  button.onclick = editMessage;
  button.innerText = 'Edit';

  m = event.target.previousElementSibling.id.split('_');
  m = parseInt(m[m.length - 1]);

  post_content = document.getElementById('content_' + m).children[0].value;

  if (post_content == undefined) {
    post_content = '';
  }

  hider = document.getElementById('hider_' + m);
  hider.style.opacity = '1';
  hider.style.visibility = 'visible';

  xhr = new XMLHttpRequest();
  xhr.open('POST', '/edit_message/' + m + '/edit', true);
  xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
  urlParams = 'post_content='+encodeURI(post_content);

  xhr.onload = function() {
    // Load the fetched HTML into the post
    message = document.getElementById('content_' + m);
    message.innerHTML = this.responseText;

    setTimeout(function() {
      hider.style.opacity = '0';
      setTimeout(function() {hider.style.visibility = 'hidden';}, 350);
    }, 100);
  }

  xhr.send(urlParams);
}

function editMessage(event) {
  button = event.target;
  m = button.previousElementSibling.id.split('_');
  m = parseInt(m[m.length - 1]);

  button.innerText = 'Save';
  button.classList.add('sidebar_button_' + currentState);
  button.onclick = saveMessage;

  message = document.getElementById('content_' + m);
  if (!message) {
    return;
  }

  paragraphs = message.children;
  text = '';

  for (i=0; i < paragraphs.length;i++) {
    text = text + paragraphs[i].innerHTML + '\n';
  }
  message.innerHTML = '';

  textarea = document.createElement('TEXTAREA');
  textarea.value = text;
  textarea.classList.add('text_' + currentState, 'textarea_' + currentState, 'reply_content');
  message.appendChild(textarea);
}
