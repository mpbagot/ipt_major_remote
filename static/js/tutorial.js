function postComment() {
  form = document.getElementsByTagName('form')[0];

  if (form == undefined || !document.getElementById('reply_content').value) {
    return;
  }

  document.getElementById('hider').style.visibility = "visible";
  document.getElementById('hider').style.opacity = "0.75";

  // dateString comes from server
  // postContent comes from server
  // userID and username come from server
  xhr = new XMLHttpRequest();
  splitURL = document.URL.split('/');
  tutorialID = splitURL[splitURL.length-2];
  xhr.open('POST', '/send_comment/'+tutorialID, true);
  xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");

  urlParams = 'comment_content='+encodeURI(document.getElementById('reply_content').value);

  xhr.onload = function() {
    fields = JSON.parse(this.responseText);
    dateString = fields.dateString;
    postContent = fields.postContent;
    userID = fields.userID;
    username = fields.username;

    // Generate the top level data
    messageBox = document.createElement('DIV');
    messageBox.classList.add('comment', 'comment_'+currentState);

    // Generate the table
    table = document.createElement('TABLE');
    tr = document.createElement('TR');

    // Generate the first cell
    cell1 = document.createElement('TD');
    cell1.classList.add('img_td');
    userImg = document.createElement('DIV');
    userImg.style.background = 'url(\'/static/img/user/' + userID + '/icon.jpg\') 50% 50% no-repeat';
    userImg.style.backgroundSize = 'cover';
    userImg.style.paddingBottom = '100%';
    userImg.style.borderRadius = '50%';
    cell1.appendChild(userImg);

    // Generate the second cell
    cell2 = document.createElement('TD');
    nameText = document.createElement('H3');
    nameText.classList.add('text_'+currentState);
    nameText.style.marginBottom = '0px';
    nameText.innerText = username;

    date = document.createElement('P');
    date.classList.add('text_'+currentState);
    date.innerText = dateString;
    date.style.margin = '5px';
    cell2.appendChild(nameText);
    cell2.appendChild(date);

    // Set cell2 styles
    cell2.style.paddingTop = '10px';
    cell2.style.verticalAlign = 'top';
    cell2.style.textAlign = 'left';

    // Construct the table
    tr.appendChild(cell1);
    tr.appendChild(cell2);
    table.appendChild(tr);
    messageBox.appendChild(table);

    // Add the comment content
    comment = document.createElement('span');
    comment.classList.add('text_'+currentState);
    comment.innerHTML = postContent;
    messageBox.appendChild(comment)

    // Set styling on the whole box
    messageBox.style.opacity = '0';
    userImg.style.opacity = '0';
    messageBox.style.padding = '0';
    messageBox.classList.add('transition');

    // Place the new message in the right spot
    hr = document.getElementById('comm_sep');
    hr.parentNode.insertBefore(messageBox, hr.nextSibling);
    height = messageBox.offsetHeight;
    messageBox.style.maxHeight = '0';
    setTimeout(function() {
      messageBox.style.maxHeight = (height+50).toString();
      messageBox.style.opacity = '1';
      messageBox.style.padding = '';
      userImg.style.opacity = '1';
    }, 30);

    form.reset()
    document.getElementById('hider').style.opacity = '0';
    setTimeout(function() {
      document.getElementById('hider').style.visibility = 'hidden';
    }, 150);
  }

  xhr.send(urlParams);

}

function deleteComment(event) {
  button = event.target;
  if (button.id == 'delete_check') {
    // Bring up the confirmation
    commentBox = button.parentNode;
    commentBox.style.transition = 'all 0.4s ease';
    commentBox.style.maxHeight = (commentBox.offsetHeight+50).toString();

    hider = button.nextElementSibling;
    hider.style.opacity = '1';
    hider.style.visibility = 'visible';

  } else {
    // Hide the confirmation
    hider = button.parentNode;
    hider.style.opacity = '0';
    setTimeout(function() {hider.style.visibility = 'hidden';}, 150);
  }

  if (button.id == 'confirm') {
    // Send the delete request, then delete the comment
    comment = button.parentNode.parentNode;
    comment.style.maxHeight = '0';
    comment.style.padding = '0';
    comment.style.opacity = '0';

    setTimeout(function() {comment.parentNode.removeChild(comment)}, 400);

    m = button.parentNode.id.split('_');
    m = m[m.length - 1];
    xhr = new XMLHttpRequest();
    xhr.open('POST', '/edit_comment/' + m + '/delete', true);
    xhr.onload = function() {
      if (this.responseText != 'success') {
        console.log(this.responseText);
      }
    }
    xhr.send();
  }
}

function saveComment(event) {
  button = event.target;
  button.classList.remove('sidebar_button_' + currentState, 'comment_button');
  button.onclick = editComment;
  button.innerText = 'Edit';

  del_button = button.nextElementSibling;
  del_button.style.display = '';

  m = event.target.previousElementSibling.id.split('_');
  m = parseInt(m[m.length - 1]);

  post_content = document.getElementById('content_' + m).children[0].value;

  if (post_content == undefined) {
    post_content = '';
  }

  hider = document.getElementById('hider_' + m);
  hider.style.opacity = '1';
  hider.style.visibility = 'visible';

  xhr = new XMLHttpRequest();
  xhr.open('POST', '/edit_comment/' + m + '/edit', true);
  xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
  urlParams = 'comment_content='+encodeURI(post_content);

  xhr.onload = function() {
    // Load the fetched HTML into the post
    message = document.getElementById('content_' + m);
    message.innerHTML = this.responseText;

    setTimeout(function() {
      hider.style.opacity = '0';
      setTimeout(function() {hider.style.visibility = 'hidden';}, 150);
    }, 100);
  }

  xhr.send(urlParams);
}

function editComment(event) {
  button = event.target;
  del_button = button.nextElementSibling;
  span = button.previousElementSibling;

  paragraphs = span.children;
  text = '';

  for (i=0; i < paragraphs.length; i++) {
    text = text + paragraphs[i].innerHTML + '\n';
  }
  span.innerHTML = '';

  textarea = document.createElement('TEXTAREA');
  textarea.value = text;
  textarea.classList.add('textarea_' + currentState, 'text_' + currentState, 'reply_content');
  span.appendChild(textarea);

  del_button.style.display = 'none';

  button.onclick = saveComment;
  button.innerText = 'Save';
  button.classList.add('sidebar_button_' + currentState, 'comment_button');
}

function deleteTutorial(event) {
  button = event.target;
  if (button.innerText == 'Click again to confirm') {
    splitURL = document.URL.split('/');
    tutorialID = splitURL[splitURL.length-2];

    xhr = new XMLHttpRequest();
    xhr.open('POST', '/delete_tutorial/' + tutorialID, true);
    xhr.onload = function() {
      window.location.href = '/tutorials';
    }
    xhr.send();
  } else {
    button.innerText = 'Click again to confirm';
  }

}
