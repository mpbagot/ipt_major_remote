document.addEventListener("DOMContentLoaded", function(){
  icons = document.getElementsByClassName('icon_link');
  document.getElementById('sidebar').addEventListener('transitionend', function() {
    for (i=0; i < icons.length; i++) {
      icons[i].style.visibility = icons[i].style.opacity == '0' ? 'hidden' : 'visible';
    }
  });
});

function toggleSidebar() {
  sidebar = document.getElementById("sidebar");
  sidebar.classList.toggle('full_aside');

  icons = document.getElementsByClassName('sidebar_icon');
  for (var e=0;e<icons.length;e++) {
    var element = icons[e];
    if (element.tagName == 'LI') {
      element.classList.toggle('full_aside');
    }
  }

  icons = document.getElementsByClassName('icon_link');
  if (sidebar.className.match('full_aside')) {
      for (var e=0;e<icons.length;e++) {
        var element = icons[e];
        element.style.transition = 'opacity .2s ease 0.15s';
        element.style.opacity = '1';
      }
  } else {
    for (var e=0;e<icons.length;e++) {
      var element = icons[e];
      element.style.opacity = '0';
      element.style.transition = 'opacity .2s ease';
    }
  }

  blocker = document.getElementById('blocker');
  blocker.style.opacity = sidebar.className.match('full_aside') ? "0.7" : "0";
  if (!sidebar.className.match('full_aside')) {
    setTimeout(function() {
      blocker.style.visibility = sidebar.className.match('full_aside') ? 'visible' : 'hidden';
    }, 300);
  } else {
    blocker.style.visibility = sidebar.className.match('full_aside') ? 'visible' : 'hidden';
  }
}
