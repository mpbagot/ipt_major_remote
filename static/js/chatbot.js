function toggleChatbot() {
  var chat = document.getElementById("chatbot_window");
  chat.classList.toggle('full_chatbot_win');
}

window.onload = function() {
  document.getElementById('chatbot_input').onkeypress = function(event) {
    // Enter key has been pressed
    if (event.keyCode == 13 && this.value != "") {
      sendMessage(this.value);
      this.value = '';
    }
  }

  document.getElementById('chatbot_send_button').onclick = function(event) {
    input = document.getElementById('chatbot_input');
    if (input.value != "") {
      sendMessage(input.value);
      input.value = "";
    }
  }
}

function sendMessage(value) {
  addMessage(value, false);

  // Send an AJAX request and parse the response into the DOM
  var xmlhttp = new XMLHttpRequest();

  xmlhttp.onload = function() {
    if (this.status == 200) {
      response = this.responseText;

      // Add the response to the message space
      addMessage(response, true);

      var space = document.getElementById('chat_message_space');

      space.scrollTop = space.scrollTopMax;
    }
  }

  xmlhttp.open("POST", "/message", true);
  xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  xmlhttp.send("message=" + value);
}

function addMessage(response, is_bot) {
  var space = document.getElementById('chat_message_space');

  var container = document.createElement('DIV');
  var message = document.createElement('DIV');
  if (is_bot) {
    message.classList.add('bot_message');
    message.classList.add('chatbot_window_'+currentState);
  } else {
    message.classList.add('user_message');
    message.classList.add("text_"+currentState);
    message.classList.add("body_"+currentState);
  }
  message.classList.add('message');
  message.innerHTML = response;

  container.appendChild(message);
  container.classList.add('message_container');
  space.appendChild(container);

}
