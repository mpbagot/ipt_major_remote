if (currentState == undefined) {
  currentState = 'light';
}


function toggleColour(rootNode) {
  if (rootNode == undefined) {
    currentState = currentState == 'light' ? 'dark' : 'light';
    rootNode = document.getElementsByTagName('body')[0];
    toggleClasses(rootNode);
  }

  for (var i=0; i<rootNode.children.length; i++) {
    toggleClasses(rootNode.children[i]);
    toggleColour(rootNode.children[i]);
  }
  return;
}

function toggleClasses(element) {
  if (element.className.match('light') != null) {
    while (element.className.match('light') != null) {
      element.className = element.className.replace('light', 'dark');
    }
  } else if (element.className.match('dark') != null) {
    while (element.className.match('dark') != null) {
      element.className = element.className.replace('dark', 'light');
    }
  }
}
