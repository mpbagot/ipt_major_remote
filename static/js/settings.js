document.addEventListener("DOMContentLoaded", function(){
    file_input = document.getElementById('file_uploader');

    file_input.onchange = function(event) {
      var fileList = file_input.files;
      if (fileList.length == 1) {
        // Switch the image around
        file = fileList[0];
        var fr = new FileReader();
        fr.onload = function () {
          document.getElementById("profile_img").src = fr.result;
        }
        fr.readAsDataURL(file);
      } else {
        // Clear the filelist
        file_input.value = null;
      }
    }
});
