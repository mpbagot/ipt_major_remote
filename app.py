#!/usr/bin/env python3
from tornado.ncss import Server
from template_language.parser import render_template
import db
import chatbot
from add_tutorial import parse_file

from threading import Thread
from multiprocessing import Process
from datetime import datetime
import shutil
import time
import os
import requests
import json

# TODO Change sidebar icons -Ed is doing this
# TODO Write tutorial - Fill in outline you've already written out
# TODO Write evaluation - Shouldn't be too hard

def logged_in(func):
    '''
    Function decorator for determining if the client is logged in
    '''
    def wrapper(request, *args):
        # Check if not logged in and return nothing
        if not get_user(request):
            not_logged_in_handler(request)
        else:
            func(request, *args)

    return wrapper

def mod_logged_in(func):
    '''
    Function decorator for determining if the client is logged in as a moderator
    '''
    def wrapper(request, *args):
        # Check if not logged in and return nothing
        if get_user(request) and get_user(request).is_moderator():
            func(request, *args)
        else:
            not_logged_in_handler(request)

    return wrapper

def get_user(request):
    '''
    Fetch the user object that the client is logged in as
    '''
    id = request.get_secure_cookie('user_id')
    if id:
        return db.User.get(int(id))
    return None

def get_theme(request):
    '''
    Get the theme in use on the client machine
    '''
    t = request.get_cookie('theme') or 'light'
    return t

def get_votd(request):
    '''
    Return whether to display the Verse-of-the-Day
    '''
    v = request.get_cookie('votd') or 'false'
    return v

def index_handler(request):
    '''
    Request handler for the website index page
    '''
    votd = ''
    if get_votd(request):
        try:
            votd = requests.get('http://labs.bible.org/api/?passage=votd&formatting=para&type=json')
            if votd.status_code == 200:
                try:
                    votd = votd.json()[0]
                except:
                    raise requests.exceptions.ConnectionError
                header = '<b>{} {}:{}</b>'.format(votd.get('bookname', ''), votd.get('chapter', ''), votd.get('verse', ''))
                votd = header + ' - ' + votd.get('text', '')
        except requests.exceptions.ConnectionError:
            votd = 'Failed to fetch Verse of the Day.'

    request.write(render_template('index.html', {"title" : "Home Page",
                                                "theme" : get_theme(request),
                                                "votd" : get_votd(request),
                                                "verseotd" : votd,
                                                "user" : get_user(request)}))

@mod_logged_in
def add_tutorial_handler(request):
    file_list = request.request.files.get('tutorial_uploader', [])
    for file_dict in file_list:
        try:
            parse_file(file_dict['body'].decode().strip())
            print(file_dict['body'].decode())
        except:
            pass

    request.redirect('/tutorials')

@mod_logged_in
def remove_tutorial_handler(request, tutorial_id):
    if not tutorial_id.isnumeric():
        request.write('')
        return

    tutorial = db.Tutorial.get(int(tutorial_id))
    tutorial.delete()

    request.write('')

def tutorial_handler(request, tutorial_index):
    '''
    Request handler for individual tutorial pages
    '''
    tutorial = db.Tutorial.get(int(tutorial_index))
    # If the tutorial doesn't exist, redirect to 404 page
    if not tutorial:
        return not_found_handler(request)

    # Otherwise, fetch the ocmments, render the HTML template and send it back
    comments = db.Comment.get_all(int(tutorial_index))
    request.write(render_template('tutorial.html', {"title" : tutorial.values['title'],
                                                    "theme" : get_theme(request),
                                                    "comments" : comments,
                                                    "tutorial" : tutorial, "user" : get_user(request)}))

def message_response_handler(request):
    '''
    Request handler for incoming messages from clients
    '''
    # Redirect GET requests to the 404 page
    if request.request.method == 'GET':
        return not_found_handler(request)

    # Otherwise get the message, process a response and send it back
    message = request.get_argument('message')
    print(message)
    user_id = int(request.get_secure_cookie('user_id') or -1)
    response = chatbot.respond(user_id, message)
    request.write(response)

def tutorial_index_handler(request):
    '''
    Request handler for the tutorial index page
    '''
    topics = db.Tutorial.get_all()
    request.write(render_template('tutorial_index.html', {"title" : "Tutorial Index", "theme" : get_theme(request),
                                                          "topics" : topics, "user" : get_user(request)}))

@logged_in
def settings_handler(request):
    '''
    Request handler for POST and GET requests to the settings page
    '''
    user = get_user(request)
    # If the client is sending settings values to the server, save them
    if request.request.method == 'POST':
        # Set the theme
        theme = {None : 'light', 'on' : 'dark'}.get(request.get_field('theme'), 'light')
        request.set_cookie('theme', theme, httponly=True, expires_days=3650)

        votd = {None : 'false', 'on' : 'true'}.get(request.get_field('votd'), 'false')
        print(request.get_field('votd'))
        request.set_cookie('votd', votd, httponly=True, expires_days=3650)

        # Set the signature
        signature = request.get_field('signature')
        user.values['signature'] = signature

        # Update the image if a new one has been uploaded
        image = request.get_file('file_uploader')
        if image[0]:
            byte_stream = open('static/img/user/{}/icon.jpg'.format(user.values['id']), 'wb')
            byte_stream.write(image[2])

        # Save the user
        user.save()
        request.redirect('/settings')
        return
    # Otherwise, return the settings HTML page
    else:
        request.write(render_template('settings.html', {'title':'User Profile',
                                                        "theme" : get_theme(request),
                                                        "votd" : get_votd(request),
                                                        "user" : get_user(request)}))

def forum_index_handler(request):
    request.write(render_template('forum_index.html', {'title' : 'Forum Index',
                                                        "topics" : db.ThreadTopic.get_all(),
                                                        "theme" : get_theme(request),
                                                        "user" : get_user(request)}))

def forum_handler(request, forum_id):
    try:
        topic_obj = db.ThreadTopic.get(int(forum_id))
        if not topic_obj:
            raise Exception
    except:
        not_found_handler(request)
        return

    threads = db.Thread.get_all(int(forum_id))

    request.write(render_template('forum.html', {'title' : topic_obj.values['title'],
                                                'theme' : get_theme(request),
                                                'threads' : threads,
                                                'topic' : topic_obj,
                                                "user" : get_user(request)}))

@logged_in
def create_thread_handler(request, forum_id):
    if request.request.method == 'POST':
        user = get_user(request)
        thread_title = request.get_field('title')
        post_content = request.get_field('post_content')

        if not all([thread_title, post_content]):
            not_found_handler(request)
            return

        new_thread = db.Thread(user.values['id'], int(forum_id), thread_title)
        new_thread.save()

        new_message = db.Message(user.values['id'], post_content, new_thread.values['id'])
        new_message.save()

        request.redirect('/forum/{}/{}/'.format(forum_id, new_thread.values['id']))
        return

    not_found_handler(request)
    return

def thread_handler(request, forum_id, thread_id):
    thread = db.Thread.get(int(thread_id))
    if not thread:
        not_found_handler(request)
        return

    request.write(render_template('thread.html', {'title' : thread.values['title'],
                                                'theme' : get_theme(request),
                                                'messages' : db.Message.get_all(int(thread_id)),
                                                "user" : get_user(request)}))

@mod_logged_in
def delete_thread_handler(request, thread_id):
    user = get_user(request)

    if not thread_id.isnumeric():
        not_found_handler(request)
        return

    thread = db.Thread.get(int(thread_id))
    topic_id = thread.values.get('topicID', 1)
    thread.delete()

    request.redirect('/forum/{}/'.format(topic_id))
    return

@logged_in
def thread_message_handler(request, thread_id):
    if request.request.method == 'POST':
        user = get_user(request)

        content = request.get_field('reply_content')

        if not all([content]):
            not_found_handler(request)
            return

        print(content)
        message = db.Message(user.values['id'], content, int(thread_id))
        message.save()

        response = {"userID" : user.values['id'],
                    "username" : user.values['username'],
                    "dateString" : db.format_date(message.values['post_date']),
                    "postContent" : db.Message.get(message.values['id']).get_content()}

        request.write(json.dumps(response))
        return

    not_found_handler(request)
    return

@logged_in
def edit_message_handler(request, message_id, action_type):
    if request.request.method == 'POST':
        user = get_user(request)
        message = db.Message.get(int(message_id))

        if action_type == 'edit':
            content = request.get_field('post_content')
            print(content)

            if content and (get_user(request).values['id'] == message.values['userID'] or get_user(request).is_moderator()):
                message.values['content'] = content

                message.save()

            request.write(db.Message.get(message.values['id']).get_content())

        elif action_type == 'delete':
            message.delete()

            request.write('success')

        return

    not_found_handler(request)
    return

@logged_in
def comment_handler(request, tutorial_id):
    if request.request.method == 'POST':
        user = get_user(request)

        content = request.get_field('comment_content')

        if not all([content]):
            not_found_handler(request)
            return

        print(content)
        comment = db.Comment(user.values['id'], content, int(tutorial_id))
        comment.save()

        response = {"userID" : user.values['id'],
                    "username" : user.values['username'],
                    "dateString" : db.format_date(comment.values['post_date']),
                    "postContent" : db.Comment.get(comment.values['id']).get_content()}

        request.write(json.dumps(response))
        return

    not_found_handler(request)
    return

@logged_in
def edit_comment_handler(request, comment_id, action_type):
    if request.request.method == 'POST':
        user = get_user(request)
        comment = db.Comment.get(int(comment_id))

        if action_type == 'edit':
            content = request.get_field('comment_content')
            print(content)

            user = get_user(request)

            if content and (user.values['id'] == comment.values['userID'] or user.is_moderator()):
                comment.values['content'] = content

                comment.save()

            request.write(comment.get_content())

        elif action_type == 'delete':
            comment.delete()

            request.write('success')

        return

    not_found_handler(request)
    return

def news_handler(request):
    try:
        r = requests.get('https://api.github.com/repos/mpbagot/A-Pi-mirror/releases')
        if r.status_code == 200:
            releases = r.json()[:10]
        else:
            raise requests.exceptions.ConnectionError('Failed to fetch')
    except requests.exceptions.ConnectionError:
        releases = []

    for r in range(len(releases)):
        releases[r]['created_at'] = db.format_date(releases[r]['created_at'].replace('T', ' ').replace('Z', ''))

    request.write(render_template('news.html', {'title' : 'A-Pi Release News',
                                                "theme" : get_theme(request),
                                                "user" : get_user(request),
                                                "releases" : releases
                                                }))

def chatbot_handler(request):
    request.write(render_template('chatbot.html', {'title' : 'Chatbot Messages',
                                                    "theme" : get_theme(request),
                                                    "user" : get_user(request)}))

def login_handler(request, error=''):
    if error:
        print(error)

    if get_user(request):
        request.redirect('/')
        return

    if request.request.method == 'GET' or error:
        request.write(render_template('login.html', {'title' : 'Login', 'theme' : get_theme(request),
                                                    'error' : error, "user" : get_user(request)}))
    elif request.request.method == 'POST':
        # Get the fields
        username = request.get_field('username')
        password = request.get_field('password')

        # Get user and check username
        db_user = db.User.get_by_username(username)
        if not db_user:
            # User doesn't exist, return error
            login_handler(request, 'There is no user with that username.')
            return

        # Check the password
        if db_user.check_password(password):
            # Login is valid, login and redirect
            request.set_secure_cookie('user_id', str(db_user.values['id']), httponly=True)
            request.redirect('/')
            return

        # Password was wrong
        login_handler(request, 'Wrong password.')

def signup_handler(request, error=''):
    if error:
        print(error)

    if get_user(request):
        request.redirect('/')
        return

    if request.request.method == 'GET' or error:
        request.write(render_template('login.html', {'title' : 'Sign-up', 'theme' : get_theme(request),
                                                    'error' : error, "user" : get_user(request)}))
    elif request.request.method == 'POST':
        # Get the fields
        username = request.get_field('username')
        password = request.get_field('password')
        confirmation_password = request.get_field('password_2')

        # Check for any issues
        if db.User.get_by_username(username) is not None:
            # Return a user exists error
            signup_handler(request, 'A user already exists with that username')
            return

        if password != confirmation_password:
            # Return a password mismatch error
            signup_handler(request, 'Passwords do not match.')
            return

        # If everything is in order, create the new user
        new_user = db.User(username, password, '')
        new_user.save()

        # Create the new profile image
        try:
            os.mkdir('static/img/user/'+str(new_user.values['id']))
        except FileExistsError:
            pass
        try:
            open('static/img/user/{}/icon.jpg'.format(new_user.values['id'])).close()
        except FileNotFoundError:
            open('static/img/user/{}/icon.jpg'.format(new_user.values['id']), 'wb').write(open('static/img/default_icon.jpg', 'rb').read())

        # Tell the client that they are logged in.
        request.set_secure_cookie('user_id', str(new_user.values['id']), httponly=True)
        request.redirect('/settings')

def logout_handler(request):
    request.clear_cookie('user_id')
    request.redirect('/')
    return

def not_logged_in_handler(request):
    if request.request.method == 'POST':
        request.write('')
        return

    message = 'You need to be logged in to see this page.' if not get_user(request) else 'You don\'t have permission to view this page.'
    request.write(render_template('error.html', {'title' : 'Error 401', 'theme' : get_theme(request),
                                                'code' : '401', 'message' : message, "user" : get_user(request)}))

def not_found_handler(request):
    request.write(render_template('error.html', {'title' : 'Error 404', 'theme' : get_theme(request),
                                                'code' : '404', 'message' : "That page doesn't seem to exist.",
                                                "user" : get_user(request)}))

def backup_handler():
    while True:
        # Make the backup asynchronously to prevent performance drop
        date = str(datetime.now()).split()[0]
        dst = "backups/database-{}.db".format(date)

        Thread(target=shutil.copy, args=["database.db", dst]).start()

        # Remove old backups
        current_date = datetime.now()
        for filename in os.listdir('backups'):
            # Parse date from filename
            date = filename[9:-3]
            old_date = datetime.strptime(date, "%Y-%m-%d")
            # Check the change in date (number of days which have passed)
            delta_date = (current_date - old_date).days
            # If greater than or equal to 30, delete the file
            if delta_date > 29:
                os.remove('backups/'+filename)

        # Wait for 24 hours
        time.sleep(60 * 60 * 24)

# Initialise the backup_handler in the background
p = Process(target=backup_handler)
p.daemon = True
p.start()

# Initialise the server
server = Server(hostname="0.0.0.0", port=80)

# Register the page handler functions
server.register('/', index_handler)
server.register('/tutorial/([0-9]+)/', tutorial_handler)
server.register('/message', message_response_handler)
server.register('/settings', settings_handler)
server.register('/tutorials', tutorial_index_handler)
server.register('/forum', forum_index_handler)
server.register('/forum/([0-9]+)/', forum_handler)
server.register('/create_thread/([0-9]+)', create_thread_handler)
server.register('/delete_thread/([0-9]+)', delete_thread_handler)
server.register('/forum/([0-9]+)/([0-9]+)/', thread_handler)
server.register('/send_message/([0-9]+)', thread_message_handler)
server.register('/edit_message/([0-9]+)/([a-z]+)', edit_message_handler)
server.register('/send_comment/([0-9]+)', comment_handler)
server.register('/edit_comment/([0-9]+)/([a-z]+)', edit_comment_handler)
server.register('/create_tutorial', add_tutorial_handler)
server.register('/delete_tutorial/([0-9]+)', remove_tutorial_handler)
server.register('/news', news_handler)
server.register('/chatbot', chatbot_handler)
server.register('/login', login_handler)
server.register('/logout', logout_handler)
server.register('/signup', signup_handler)
server.register('/.*', not_found_handler)

server.run()
